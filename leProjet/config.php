<?php
class		Config
{
	var		$version = '0.1';
	var		$debug = true; //desactive le cache et affiche les messages dsm
	var		$dsm = true; //si false, n'affiche pas les dsm meme en debug
	var		$session_duration = 18000; //5*60*60 = 5 hours
	
    var     $contact_adress = array('', '');
	var		$site_name = 'Review SixFellows';

	var		$db_server = 'localhost';
	var		$db_name = 'review';
	var		$db_user = 'root';
	var		$db_password = '';
};

@$config = new Config();
?>
