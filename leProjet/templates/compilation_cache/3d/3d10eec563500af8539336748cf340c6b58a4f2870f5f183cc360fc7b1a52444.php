<?php

/* menu.html */
class __TwigTemplate_a66914c12c3f66e9114c041f0b7a2e4b4dfeeb80019ad7f90d35264ddaea7fd9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"";
        echo twig_escape_filter($this->env, (isset($context["template"]) ? $context["template"] : null), "html", null, true);
        echo "\">
    <div id=\"logo\">
        <div class=\"row-fluid\">
            <div class=\"col-sm-3\">
                <a href=\"";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["base_path"]) ? $context["base_path"] : null), "html", null, true);
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, (isset($context["base_path"]) ? $context["base_path"] : null), "html", null, true);
        echo "images/perjeta.png\" class=\"img-responsive\" alt=\"Logo\"></a>
            </div>
            <div class=\"col-sm-6\">
            </div>
            <div class=\"col-sm-3\">
                <!-- <div class=\"search-box\"> -->
                    <!-- <p>Search <span class=\"glyphicon glyphicon-search\"></span></p> -->
                <!-- </div> -->
            </div>
        </div>
    </div>

    <nav class=\"header-menu navbar\">
        <div class=\"container-fluid\">
            <div class=\"navbar-header\">
                <a class=\"navbar-brand\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["base_path"]) ? $context["base_path"] : null), "html", null, true);
        echo "\">SixFellows</a>
            </div>
            <ul class=\"nav navbar-nav\">
                <li class=\"";
        // line 23
        if (((isset($context["template"]) ? $context["template"] : null) == "example")) {
            echo "active";
        }
        echo "\"><a href=\"example\">Example</a></li>
                <li class=\"";
        // line 24
        if (((isset($context["template"]) ? $context["template"] : null) == "test")) {
            echo "active";
        }
        echo "\"><a href=\"test\">Test</a></li>
                <li class=\"";
        // line 25
        if (((isset($context["template"]) ? $context["template"] : null) == "test-example")) {
            echo "active";
        }
        echo "\"><a href=\"test-example\">Test-example</a></li> 
                <li class=\"";
        // line 26
        if (((isset($context["template"]) ? $context["template"] : null) == "review")) {
            echo "active";
        }
        echo "\"><a href=\"review\">Review</a></li>
                <li class=\"";
        // line 27
        if (((isset($context["template"]) ? $context["template"] : null) == "postApplication")) {
            echo "active";
        }
        echo "\"><a href=\"posteApplication\">Poster une application</a></li>

                ";
        // line 29
        if (($this->getAttribute((isset($context["account"]) ? $context["account"] : null), "uid", array()) != 0)) {
            // line 30
            echo "                    <li class=\"";
            if (((isset($context["template"]) ? $context["template"] : null) == "mesApplications")) {
                echo "active";
            }
            echo "\"><a href=\"mesApplications\">Mes applications</a></li>
                    <li id=\"login\"><a href=\"?q=logout\">Logout</a>
                ";
        }
        // line 33
        echo "                ";
        if (($this->getAttribute((isset($context["account"]) ? $context["account"] : null), "uid", array()) == 0)) {
            // line 34
            echo "                    <li class=\"dropdown\" id=\"login\">
                        <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"\">Login
                            <span class=\"caret\"></span>
                        </a>
                        <ul class=\"dropdown-menu\">
                            <li id=\"login\"><a href=\"?q=loginFacebook\">with Facebook</a></li>
                            <li id=\"login\"><a href=\"?q=loginTwitter\">with Twitter</a></li>                        
                            ";
        }
        // line 42
        echo "                        </ul>
                    </li>
            </ul>
        </div>
    </nav>
</div>";
    }

    public function getTemplateName()
    {
        return "menu.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 42,  98 => 34,  95 => 33,  86 => 30,  84 => 29,  77 => 27,  71 => 26,  65 => 25,  59 => 24,  53 => 23,  47 => 20,  27 => 5,  19 => 1,);
    }
}
/* <div class="{{template}}">*/
/*     <div id="logo">*/
/*         <div class="row-fluid">*/
/*             <div class="col-sm-3">*/
/*                 <a href="{{base_path}}"><img src="{{base_path}}images/perjeta.png" class="img-responsive" alt="Logo"></a>*/
/*             </div>*/
/*             <div class="col-sm-6">*/
/*             </div>*/
/*             <div class="col-sm-3">*/
/*                 <!-- <div class="search-box"> -->*/
/*                     <!-- <p>Search <span class="glyphicon glyphicon-search"></span></p> -->*/
/*                 <!-- </div> -->*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* */
/*     <nav class="header-menu navbar">*/
/*         <div class="container-fluid">*/
/*             <div class="navbar-header">*/
/*                 <a class="navbar-brand" href="{{base_path}}">SixFellows</a>*/
/*             </div>*/
/*             <ul class="nav navbar-nav">*/
/*                 <li class="{% if template == 'example' %}active{% endif %}"><a href="example">Example</a></li>*/
/*                 <li class="{% if template == 'test' %}active{% endif %}"><a href="test">Test</a></li>*/
/*                 <li class="{% if template == 'test-example' %}active{% endif %}"><a href="test-example">Test-example</a></li> */
/*                 <li class="{% if template == 'review' %}active{% endif %}"><a href="review">Review</a></li>*/
/*                 <li class="{% if template == 'postApplication' %}active{% endif %}"><a href="posteApplication">Poster une application</a></li>*/
/* */
/*                 {% if(account.uid != 0) %}*/
/*                     <li class="{% if template == 'mesApplications' %}active{% endif %}"><a href="mesApplications">Mes applications</a></li>*/
/*                     <li id="login"><a href="?q=logout">Logout</a>*/
/*                 {% endif %}*/
/*                 {% if(account.uid == 0) %}*/
/*                     <li class="dropdown" id="login">*/
/*                         <a class="dropdown-toggle" data-toggle="dropdown" href="">Login*/
/*                             <span class="caret"></span>*/
/*                         </a>*/
/*                         <ul class="dropdown-menu">*/
/*                             <li id="login"><a href="?q=loginFacebook">with Facebook</a></li>*/
/*                             <li id="login"><a href="?q=loginTwitter">with Twitter</a></li>                        */
/*                             {% endif %}*/
/*                         </ul>*/
/*                     </li>*/
/*             </ul>*/
/*         </div>*/
/*     </nav>*/
/* </div>*/
