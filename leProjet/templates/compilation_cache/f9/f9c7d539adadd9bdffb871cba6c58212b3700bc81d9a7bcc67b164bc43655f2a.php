<?php

/* footer.html */
class __TwigTemplate_d27d5772791a7cd0b4c54565586076ba02b4e7dcd980720304dbea2f16de7749 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"footer-top\">
    <div class=\"container\">
        <div class=\"row-fluid\">
            <div class=\"col-sm-4\">
                <h1>About</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus hic accusantium provident nostrum officia, et laudantium aperiam culpa aliquam, ducimus qui aut voluptatem repellat quidem, autem cum id similique, cumque.</p>
            </div>
            <div class=\"col-sm-4\">
                <h1>Recent Tweets</h1>
                <div class=\"tweet\">
                    <ul>
                    ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tweets"]) ? $context["tweets"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["tweet"]) {
            // line 13
            echo "                        <li>
                            ";
            // line 14
            echo twig_escape_filter($this->env, $context["tweet"], "html", null, true);
            echo "
                        </li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tweet'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 17
        echo "                    </ul>
                </div>
                    <div class=\"col-sm-12 text-center\">
                        <img src=\"";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["base_path"]) ? $context["base_path"] : null), "html", null, true);
        echo "images/triangle-footer.png\">
                    </div>
                </div>
            </div>
            <div class=\"col-sm-2\">
                <h1>Sections</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus eligendi quos</p>
            </div>
            <div class=\"col-sm-2\">
                <h1>Categories</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus eligendi quos</p>
            </div>
        </div>
    </div>
</div>
<div id=\"footer-bottom\">
    <div class=\"container\">
        <div class=\"row-fluid\">
            <div class=\"col-sm-4\">
                <p class=\"text-left\">Responsiver</p>
            </div>
            <div class=\"col-sm-4\">
                <p class=\"text-center\">2016 Copyrights ...</p>
            </div>
            <div class=\"col-sm-4\">
                <p class=\"text-right\">
                    <a href=\"#\"><img src=\"";
        // line 46
        echo twig_escape_filter($this->env, (isset($context["base_path"]) ? $context["base_path"] : null), "html", null, true);
        echo "images/twitter-icone.png\"></a>
                    <a href=\"#\"><img src=\"";
        // line 47
        echo twig_escape_filter($this->env, (isset($context["base_path"]) ? $context["base_path"] : null), "html", null, true);
        echo "images/linkedin-icone.png\"></a>
                    <a href=\"#\"><img src=\"";
        // line 48
        echo twig_escape_filter($this->env, (isset($context["base_path"]) ? $context["base_path"] : null), "html", null, true);
        echo "images/facebook-icone.png\"></a>
                    <a href=\"#\"><img src=\"";
        // line 49
        echo twig_escape_filter($this->env, (isset($context["base_path"]) ? $context["base_path"] : null), "html", null, true);
        echo "images/vimeo-icone.png\"></a>
                </p>
            </div>
        </div>
    </div>
</div>

<script type=\"text/javascript\"> 
            interval = \"\";
            function testtt(id){
                  clearInterval(interval);
                                
            }
            lauchcarousel('0');
            function lauchcarousel(id){
              if(parseInt(id)==5){id = 0;}
              clearTimeout(interval);
              \$(\".indicator li\" ).removeClass( \"active2\" )

              \$(\".indicator li:eq(\"+id+\")\").addClass('active2');

              if(id == 0)
                \$(\".tweet ul\").animate({marginTop:0},500);
              if(id == 1)
                \$(\".tweet ul\").animate({marginTop:-110},500);
              if(id == 2)
                 \$(\".tweet ul\").animate({marginTop:-240},500);
              if(id == 3)
                 \$(\".tweet ul\").animate({marginTop:-390},500);
              if(id == 4)
                 \$(\".tweet ul\").animate({marginTop:-510},500);
              id = parseInt(id);
              if(parseInt(id)<=4){id = id+1;}else{id = 0;}
              interval = setTimeout(function(){ console.log(id);lauchcarousel(id); }, 4000);

            }
        </script>";
    }

    public function getTemplateName()
    {
        return "footer.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 49,  90 => 48,  86 => 47,  82 => 46,  53 => 20,  48 => 17,  39 => 14,  36 => 13,  32 => 12,  19 => 1,);
    }
}
/* <div id="footer-top">*/
/*     <div class="container">*/
/*         <div class="row-fluid">*/
/*             <div class="col-sm-4">*/
/*                 <h1>About</h1>*/
/*                 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus hic accusantium provident nostrum officia, et laudantium aperiam culpa aliquam, ducimus qui aut voluptatem repellat quidem, autem cum id similique, cumque.</p>*/
/*             </div>*/
/*             <div class="col-sm-4">*/
/*                 <h1>Recent Tweets</h1>*/
/*                 <div class="tweet">*/
/*                     <ul>*/
/*                     {% for tweet in tweets %}*/
/*                         <li>*/
/*                             {{tweet}}*/
/*                         </li>*/
/*                     {% endfor %}*/
/*                     </ul>*/
/*                 </div>*/
/*                     <div class="col-sm-12 text-center">*/
/*                         <img src="{{base_path}}images/triangle-footer.png">*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*             <div class="col-sm-2">*/
/*                 <h1>Sections</h1>*/
/*                 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus eligendi quos</p>*/
/*             </div>*/
/*             <div class="col-sm-2">*/
/*                 <h1>Categories</h1>*/
/*                 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus eligendi quos</p>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* <div id="footer-bottom">*/
/*     <div class="container">*/
/*         <div class="row-fluid">*/
/*             <div class="col-sm-4">*/
/*                 <p class="text-left">Responsiver</p>*/
/*             </div>*/
/*             <div class="col-sm-4">*/
/*                 <p class="text-center">2016 Copyrights ...</p>*/
/*             </div>*/
/*             <div class="col-sm-4">*/
/*                 <p class="text-right">*/
/*                     <a href="#"><img src="{{base_path}}images/twitter-icone.png"></a>*/
/*                     <a href="#"><img src="{{base_path}}images/linkedin-icone.png"></a>*/
/*                     <a href="#"><img src="{{base_path}}images/facebook-icone.png"></a>*/
/*                     <a href="#"><img src="{{base_path}}images/vimeo-icone.png"></a>*/
/*                 </p>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* */
/* <script type="text/javascript"> */
/*             interval = "";*/
/*             function testtt(id){*/
/*                   clearInterval(interval);*/
/*                                 */
/*             }*/
/*             lauchcarousel('0');*/
/*             function lauchcarousel(id){*/
/*               if(parseInt(id)==5){id = 0;}*/
/*               clearTimeout(interval);*/
/*               $(".indicator li" ).removeClass( "active2" )*/
/* */
/*               $(".indicator li:eq("+id+")").addClass('active2');*/
/* */
/*               if(id == 0)*/
/*                 $(".tweet ul").animate({marginTop:0},500);*/
/*               if(id == 1)*/
/*                 $(".tweet ul").animate({marginTop:-110},500);*/
/*               if(id == 2)*/
/*                  $(".tweet ul").animate({marginTop:-240},500);*/
/*               if(id == 3)*/
/*                  $(".tweet ul").animate({marginTop:-390},500);*/
/*               if(id == 4)*/
/*                  $(".tweet ul").animate({marginTop:-510},500);*/
/*               id = parseInt(id);*/
/*               if(parseInt(id)<=4){id = id+1;}else{id = 0;}*/
/*               interval = setTimeout(function(){ console.log(id);lauchcarousel(id); }, 4000);*/
/* */
/*             }*/
/*         </script>*/
