<?php

/* review.html */
class __TwigTemplate_49ec515dcb98083f00243058cd9f15cce4e599c2243007363e9de7ac5e0f51aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "﻿<div class=\"";
        echo twig_escape_filter($this->env, (isset($context["template"]) ? $context["template"] : null), "html", null, true);
        echo "\">
    <div class=\"container\">
        <div class=\"col-sm-12\">
            <h1>Faire une Review :</h1>
            ";
        // line 5
        if (($this->getAttribute((isset($context["account"]) ? $context["account"] : null), "uid", array()) != null)) {
            // line 6
            echo "                ";
            if (($this->getAttribute((isset($context["content"]) ? $context["content"] : null), "application", array()) != null)) {
                // line 7
                echo "                    <p class=\"text-center\">
                        <img src=\"";
                // line 8
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["content"]) ? $context["content"] : null), "application", array()), "results", array()), 0, array()), "artworkUrl512", array()), "html", null, true);
                echo "\">
                        <p>";
                // line 9
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["content"]) ? $context["content"] : null), "application", array()), "results", array()), 0, array()), "description", array()), "html", null, true);
                echo "</p>
                    </p>
                    <hr>
                    <form method=\"post\" action=\"review\" enctype=\"multipart/form-data\">
                        <div class=\"form-group\">
                            <label for=\"fichier\">Screenshot de la review</label>
                            <input type=\"file\" id=\"fichier\" name=\"fichier\">
                        </div>
                        <div class=\"col-sm-offset-2 col-sm-10\">
                            <button type=\"submit\" name=\"submit\" class=\"btn btn-primary\">Submit</button>
                        </div>
                    </form>
                ";
            } else {
                // line 22
                echo "                    <p>
                        Aucune application à review pour le moment !
                    </p>
                ";
            }
            // line 26
            echo "            ";
        } else {
            // line 27
            echo "                <p>
                    Connectez-vous avant de faire une review !
                </p>
            ";
        }
        // line 31
        echo "        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "review.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 31,  64 => 27,  61 => 26,  55 => 22,  39 => 9,  35 => 8,  32 => 7,  29 => 6,  27 => 5,  19 => 1,);
    }
}
/* ﻿<div class="{{template}}">*/
/*     <div class="container">*/
/*         <div class="col-sm-12">*/
/*             <h1>Faire une Review :</h1>*/
/*             {% if account.uid != null %}*/
/*                 {% if content.application != null %}*/
/*                     <p class="text-center">*/
/*                         <img src="{{content.application.results.0.artworkUrl512}}">*/
/*                         <p>{{content.application.results.0.description}}</p>*/
/*                     </p>*/
/*                     <hr>*/
/*                     <form method="post" action="review" enctype="multipart/form-data">*/
/*                         <div class="form-group">*/
/*                             <label for="fichier">Screenshot de la review</label>*/
/*                             <input type="file" id="fichier" name="fichier">*/
/*                         </div>*/
/*                         <div class="col-sm-offset-2 col-sm-10">*/
/*                             <button type="submit" name="submit" class="btn btn-primary">Submit</button>*/
/*                         </div>*/
/*                     </form>*/
/*                 {% else %}*/
/*                     <p>*/
/*                         Aucune application à review pour le moment !*/
/*                     </p>*/
/*                 {% endif %}*/
/*             {% else %}*/
/*                 <p>*/
/*                     Connectez-vous avant de faire une review !*/
/*                 </p>*/
/*             {% endif %}*/
/*         </div>*/
/*     </div>*/
/* </div>*/
