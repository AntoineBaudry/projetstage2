<?php

/* html.html */
class __TwigTemplate_9e41759dc4a7bbe4d4013d3d065a61303880344d1758f700805ecf3d8e0872df extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "﻿<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <title>";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        echo "</title>
    ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["head"]) ? $context["head"] : null), "css", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["css"]) {
            // line 9
            echo "    <link href=\"";
            echo twig_escape_filter($this->env, ((isset($context["base_path"]) ? $context["base_path"] : null) . $context["css"]), "html", null, true);
            echo "\" rel=\"stylesheet\">
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['css'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "
    ";
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["head"]) ? $context["head"] : null), "js", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["js"]) {
            // line 13
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, ((isset($context["base_path"]) ? $context["base_path"] : null) . $context["js"]), "html", null, true);
            echo "\"></script>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['js'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->
</head>
<body>
    <noscript>
        <div class=\"modal-backdrop\"></div>
        <div class=\"modal show\" id=\"javascript-disabled\" tabindex=\"-1\" role=\"dialog\">
            <div class=\"modal-dialog\" role=\"document\">
                <div class=\"modal-content\">
                    <div class=\"modal-header\">
                        <h4>JavaScript est désactivé dans votre navigateur Internet</h4>
                    </div>
                    <div class=\"modal-body\">
                        <p>
                            Vous devez activer JavaScript pour continuer votre navigation.
                        </p>
                        <p>Ensuite, actualiser cette page pour supprimer ce message.</p>
                        <p>
                            <a href=\"http://www.enable-javascript.com/fr/\" target=\"_blanck\">Comment activer JavaScript</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </noscript>

    <div id=\"header\">
        <div class=\"container\">";
        // line 44
        $this->loadTemplate("header.html", "html.html", 44)->display($context);
        echo "</div>
    </div>

    <div id=\"header-menu\">
        <div class=\"container\">";
        // line 48
        $this->loadTemplate("menu.html", "html.html", 48)->display($context);
        echo "</div>
    </div>

    <div id=\"main-container\">
        ";
        // line 52
        if ((twig_length_filter($this->env, (isset($context["messages"]) ? $context["messages"] : null)) > 0)) {
            // line 53
            echo "        <div id=\"messages\" class=\"container\">
            <div class=\"row\">
                <!-- <div class=\"";
            // line 55
            if (((isset($context["template"]) ? $context["template"] : null) == "home")) {
                echo "col-md-12";
            } else {
                echo "col-md-10 col-md-offset-1";
            }
            echo "\">
                -->
                <div class=\"col-md-12\">
                    ";
            // line 58
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["messages"]) ? $context["messages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 59
                echo "                        ";
                // line 60
                echo "                    <div class=\"alert alert-";
                echo $this->getAttribute($context["message"], "type", array());
                echo " alert-dismissible\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                            <span aria-hidden=\"true\">&times;</span>
                        </button> <strong>";
                // line 63
                echo $this->getAttribute($context["message"], "title", array());
                echo "</strong>
                        ";
                // line 64
                echo $this->getAttribute($context["message"], "text", array());
                echo "
                    </div>
                    ";
                // line 67
                echo "                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 68
            echo "                </div>
            </div>
        </div>
        ";
        }
        // line 72
        echo "
        <!-- <div class=\"container\"> -->
            <div class=\"row\">
                <!-- <div id=\"content\" class=\"";
        // line 75
        if (((isset($context["template"]) ? $context["template"] : null) == "home")) {
            echo "col-md-12";
        } else {
            echo "col-md-10 col-md-offset-1";
        }
        echo "\">
                -->
                <div id=\"content\" class=\"col-md-12\">";
        // line 77
        $this->loadTemplate((isset($context["template_file"]) ? $context["template_file"] : null), "html.html", 77)->display($context);
        echo "</div>
            </div>
        <!-- </div> -->
    </div>

    <div id=\"footer\">       
            ";
        // line 83
        $this->displayBlock('footer', $context, $blocks);
        // line 86
        echo "    </div>
</body>
</html>";
    }

    // line 83
    public function block_footer($context, array $blocks = array())
    {
        // line 84
        echo "                ";
        $this->loadTemplate("footer.html", "html.html", 84)->display($context);
        // line 85
        echo "            ";
    }

    public function getTemplateName()
    {
        return "html.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  193 => 85,  190 => 84,  187 => 83,  181 => 86,  179 => 83,  170 => 77,  161 => 75,  156 => 72,  150 => 68,  144 => 67,  139 => 64,  135 => 63,  128 => 60,  126 => 59,  122 => 58,  112 => 55,  108 => 53,  106 => 52,  99 => 48,  92 => 44,  61 => 15,  52 => 13,  48 => 12,  45 => 11,  36 => 9,  32 => 8,  28 => 7,  20 => 1,);
    }
}
/* ﻿<!DOCTYPE html>*/
/* <html lang="en">*/
/* <head>*/
/*     <meta charset="utf-8">*/
/*     <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*     <title>{{ title }}</title>*/
/*     {% for css in head.css %}*/
/*     <link href="{{ base_path ~ css }}" rel="stylesheet">*/
/*     {% endfor %}*/
/* */
/*     {% for js in head.js %}*/
/*     <script type="text/javascript" src="{{ base_path ~ js }}"></script>*/
/*     {% endfor %}*/
/*     <!--[if lt IE 9]>*/
/*     <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>*/
/*     <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>*/
/*     <![endif]-->*/
/* </head>*/
/* <body>*/
/*     <noscript>*/
/*         <div class="modal-backdrop"></div>*/
/*         <div class="modal show" id="javascript-disabled" tabindex="-1" role="dialog">*/
/*             <div class="modal-dialog" role="document">*/
/*                 <div class="modal-content">*/
/*                     <div class="modal-header">*/
/*                         <h4>JavaScript est désactivé dans votre navigateur Internet</h4>*/
/*                     </div>*/
/*                     <div class="modal-body">*/
/*                         <p>*/
/*                             Vous devez activer JavaScript pour continuer votre navigation.*/
/*                         </p>*/
/*                         <p>Ensuite, actualiser cette page pour supprimer ce message.</p>*/
/*                         <p>*/
/*                             <a href="http://www.enable-javascript.com/fr/" target="_blanck">Comment activer JavaScript</a>*/
/*                         </p>*/
/*                     </div>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </noscript>*/
/* */
/*     <div id="header">*/
/*         <div class="container">{% include 'header.html' %}</div>*/
/*     </div>*/
/* */
/*     <div id="header-menu">*/
/*         <div class="container">{% include 'menu.html' %}</div>*/
/*     </div>*/
/* */
/*     <div id="main-container">*/
/*         {% if messages|length > 0 %}*/
/*         <div id="messages" class="container">*/
/*             <div class="row">*/
/*                 <!-- <div class="{% if template == 'home' %}col-md-12{% else %}col-md-10 col-md-offset-1{% endif %}">*/
/*                 -->*/
/*                 <div class="col-md-12">*/
/*                     {% for message in messages %}*/
/*                         {% autoescape false %}*/
/*                     <div class="alert alert-{{ message.type }} alert-dismissible" role="alert">*/
/*                         <button type="button" class="close" data-dismiss="alert" aria-label="Close">*/
/*                             <span aria-hidden="true">&times;</span>*/
/*                         </button> <strong>{{ message.title }}</strong>*/
/*                         {{ message.text }}*/
/*                     </div>*/
/*                     {% endautoescape %}*/
/*                     {% endfor %}*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*         {% endif %}*/
/* */
/*         <!-- <div class="container"> -->*/
/*             <div class="row">*/
/*                 <!-- <div id="content" class="{% if template == 'home' %}col-md-12{% else %}col-md-10 col-md-offset-1{% endif %}">*/
/*                 -->*/
/*                 <div id="content" class="col-md-12">{% include template_file %}</div>*/
/*             </div>*/
/*         <!-- </div> -->*/
/*     </div>*/
/* */
/*     <div id="footer">       */
/*             {% block footer %}*/
/*                 {% include 'footer.html' %}*/
/*             {% endblock %}*/
/*     </div>*/
/* </body>*/
/* </html>*/
