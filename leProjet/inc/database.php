<?php
/**
 * cette classe s'occupe de toutes les requêtes SQL à traiter
 */
class					Database
{
    /**
     * database
     * @var database
     */
    var                 $db;
    /**
     * model
     * @var model
     */
    var                 $m;

    /**
     * constructeur de la classe database,
     * initialise les attributs $m et $db (en se connectant à la base)
     * @param model &$model model
     */
	function			Database(&$model)
	{
		global			$config;

        $this->m = $model;

        $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION; //emet une exeption en cas d'erreur
		$pdo_options[PDO::ATTR_DEFAULT_FETCH_MODE ] = PDO::FETCH_ASSOC;

        //connexion PDO
        try 
        {
            $dns = 'mysql:dbname='.$config->db_name.';host='.$config->db_server;
            //objet global PDO
            $db = new PDO($dns, $config->db_user, $config->db_password, $pdo_options);
            
            $db->query("SET NAMES 'utf8'");

            $this->db = $db;
        }
        catch (PDOException $e) 
        {
            // echo 'Connexion échouée : ' . $e->getMessage();
            echo 'Failed to join DB';
            exit();
        }
	}

    //enregistre un nouveau log
    /**
     * requête permettant d'enregistrer un nouveau log
     * @param  string $page    une page du site
     * @param  string $comment un commentaire
    */
    function            logNew($page, $comment = '')
    {
        $sql = 'INSERT INTO logs(page, session, created, comment)
                VAlUES(:page, :session, :created, :comment)';
            
        $datas = array(':page' => $page, ':session' => session_id(), ':created' => time(), ':comment' => $comment);

        // $this->m->dsm($this->dq($sql, $datas));
    
        $req = $this->db->prepare($sql);
        $req->execute($datas);
    }

    /**
     * requête permettant de retourner les logs déjà existant
     * @return array tableau contenant les logs
     */
    function            logLoad()
    {
        $sql = 'SELECT page, count(DISTINCT session) as view
                FROM logs as l
                GROUP BY page';
            
        $datas = array();
    
        $req = $this->db->prepare($sql);
        $req->execute($datas);

        $res = $req->fetchAll();

        return $res;
    }

    //display query (dq)
    //affiche une requete sql prepare par PDO
    function dq($sql, $datas)
    {
        $k = array();
        $d = array();
        
        foreach($datas as $key => $val)
        {
            $k[] = $key;
            $d[] = "'".$val."'";
        }
        
        return str_replace($k, $d, $sql);
    }

    /**
     * requête permettant d'ajouter dans la base de données un nouvel utilisateur
     * @param  string $type source de la connexion (Facebook ou Twitter)
     * @param  string $id   le facebook ou twitter Id de l'utilisateur connecté
     * @return bool       Vrai si l'utilisateur a été ajouté, Faux si il y a eu une erreur
     */
    function newUser($type, $id)
    {
        $sql="";
        if ($type == "facebook") {
            $sql = "INSERT INTO users(facebookId, type, points)
                    VALUES (:id, :type, :points)";
        }
        elseif ($type == "twitter") {
            $sql = "INSERT INTO users (twitterId, type, points)
                    VALUES (:id, :type, :points)";
        }

        $datas = array(
            ":id" => $id,
            ":type" => $type,
            ":points" => 0
        );

        $req = $this->db->prepare($sql);
        $res = $req->execute($datas);

        return $res;
    }

    /**
     * requête permettant de récupérer tous les utilisateurs de la base de données
     * ou récupérer un utilisateur en utilisant un filtre sur la requête SQL
     * @param  array  $fields tableau filtre pour la requête SQL
     * @return array  $res      tableau contenant de 0 à plusieurs accounts 
     */
    function getUsers($fields = array())
    {
        $sql = "SELECT uid, facebookId, twitterId, points
                FROM users
                WHERE 1";
        
        $datas = $fields;

        foreach ($fields as $key => $value) {
            $sql .= " AND " . $key . " = :" . $key;
            $datas[':'.$key] = $value;
        }

        $req = $this->db->prepare($sql);
        $req->execute($datas);

        $res = $req->fetchAll();

        return $res;
    }

    /**
     * requête permettant de retourner les applications qui peuvent être review,
     * le créateur de l'application doit avoir plus de 0 point et un utilisateur
     * ne peut pas tomber sur ses applications
     * @param  string $uid identifiant unique de l'account connecté
     * @return array      tableau contenant de 0 à plusieurs application
     */
    function getApplicationForReview($uid) {
        $sql = "SELECT aid, nom, lien, datePublication, uid
                FROM application
                WHERE uid in (SELECT uid FROM users WHERE points > 0 AND uid <> :uid)";

        $datas = array(':uid' => $uid);

        $req = $this->db->prepare($sql);
        $req->execute($datas);

        $res = $req->fetchAll();

        return $res;
    }

    /**
     * requête permettant de retourner les applications de la base de données
     * @param  array  $fields tableau filtre pour la requête SQL
     * @return array         tableau contenant de 0 à plusieurs applications 
     */
    function getApplications($fields = array()) {
        $sql = "SELECT aid, nom, lien, datePublication, uid
                FROM application
                WHERE 1";
        
        $datas = $fields;

        foreach ($fields as $key => $value) {
            $sql .= " AND " . $key . " = :" . $key;
            $datas[':'.$key] = $value;
        }

        $req = $this->db->prepare($sql);
        $req->execute($datas);

        $res = $req->fetchAll();

        return $res;
    }

    /**
     * requête permettant de trouver une application par rapport à son URL (lien)
     * @param  string $lien bout du lien de l'application ou se situe le nom de l'application
     * @return array       tableau contenant de 0 à 1 application
     */
    function getApplicationByLien($lien) {
        $sql = "SELECT aid, nom, lien, datePublication, uid
                FROM application
                WHERE lien LIKE :lien";
                
        $datas = array(':lien' => $lien);

        $req = $this->db->prepare($sql);
        $req->execute($datas);

        $res = $req->fetchAll();

        return $res;
    }

    /**
     * requête permettant de supprimer une application de la base de données
     * @param  int $aid identifiant unique de l'application
     * @return bool      Vrai si l'application a été supprimée, Faux si non
     */
    function supprimerApplication($aid) {
        $sql = "DELETE FROM application
                WHERE aid = :aid";

        $datas = array(':aid' => $aid);

        $req = $this->db->prepare($sql);
        $res = $req->execute($datas);

        return $res;
    }

    /**
     * requête permettant d'ajouter dans la base de données une nouvelle application
     * @param  string $nom  nom de l'application
     * @param  string $lien lien vers l'AppStore de l'application
     * @return bool       Vrai si l'application a été ajoutée, Faux si l'application n'a pas pu être ajoutée
     */
    function posteApplication($nom, $lien) {
        $sql = "INSERT INTO application(nom, lien, datePublication, uid)
                VALUES (:nom, :lien, :datePublication, :uid)";

        $datas = array(
            ":nom" => $nom,
            ":lien" => $lien,
            ":datePublication" => date("Y-m-d"),
            ":uid" => $this->m->account->uid
        );

        $req = $this->db->prepare($sql);
        $res = $req->execute($datas);

        return $res;
    }

    /**
     * requête permettant de retourner les review de la base de données
     * @param  array  $fields tableau de filtre pour la requête SQL
     * @return array         tableau contenant de 0 à plusieurs reviews
     */
    function getReview($fields = array()) {
        $sql = "SELECT rid, aid, uid, screenshot
                FROM review
                WHERE 1";

        $datas = $fields;

        foreach ($fields as $key => $value) {
            $sql .= " AND " . $key . " = :" . $key;
            $datas[':'.$key] = $value;
        }

        $req = $this->db->prepare($sql);
        $req->execute($datas);

        $res = $req->fetchAll();

        return $res;
    }

    /**
     * requête permettant d'ajouter une review dans la base de données
     * @param  int $aid identifiant unique de l'application
     * @param  int $uid identifiant unique de l'account
     * @return bool      Vrai si la review a été ajoutée, Faux si la review n'a pas pu être ajoutée 
     */
    function ajouterReview($aid, $uid) {

        $sql = "INSERT INTO review(aid, uid, screenshot)
                VALUES (:aid, :uid, :screenshot)";

        $datas = array(
            ":aid" => $aid,
            ":uid" => $uid,
            ":screenshot" => 1
        );

        $req = $this->db->prepare($sql);
        $res = $req->execute($datas);

        return $res;
    }

    /**
     * requête permettant d'ajouter un point à l'account
     * @param  int $uid identifiant unique de l'account
     * @return bool      Vrai si le point a été ajouté, Faux si le point n'a pas été ajouté
     */
    function ajouterPoint($uid) {

        $requete1 = current($this->getUsers(array('uid' => $uid)));
        $points = $requete1['points'] + 1;

        $sql = "UPDATE users
                SET points = :points
                WHERE uid = :uid";

        $datas = array(
            ":points" => $points,
            ":uid" => $uid
        );

        $req = $this->db->prepare($sql);
        $res = $req->execute($datas);

        return $res;
    }

    /**
     * requête permettant d'enlever un point à l'account qui  fait l'application qui a été review
     * @param  int $uid identifiant unique de l'account
     * @return bool      Vrai si le point a été enlevé, Faux si le point n'a pas été enlevé
     */
    function enleverPoint($uid) {

        $requete1 = current($this->getUsers(array('uid' => $uid)));
        $points = $requete1['points'] - 1;

        $sql = "UPDATE users
                SET points = :points
                WHERE uid = :uid";

        $datas = array(
            ":points" => $points,
            ":uid" => $uid
        );

        $req = $this->db->prepare($sql);
        $res = $req->execute($datas);

        return $res;
    }
}
?>
