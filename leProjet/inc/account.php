<?php
/**
 * cette classe permet de gérer une personne qui se connecte
 * en récupérant son uid, son facebookId/twitterId et ses points
 * et en les ajoutant dans les attributs correspondants
 */
class                   Account
{
    /**
     * model
     * @var model
     */
    var                 $m;
    
    /**
     * database
     * @var database
     */
    var                 $db;

    /**
     * identifiant unique d'un utilisateur
     * @var string
     */
    var                 $uid;
    /**
     * l'id facebook de l'utilisateur connecté
     * @var string
     */
    var                 $facebookId;
    /**
     * l'id twitter de l'utilisateur connecté
     * @var string
     */
    var                 $twitterId;
    /**
     * les points de l'utilisateur connecté
     * @var string
     */
    var                 $points;
             
    /**
     * constructeur de la classe Account
     * @param model &$model model
     */
    function            Account(&$model)
    {
        $this->m = $model;
        $this->db = $this->m->db;

        if (isset($_SESSION['uid'])) {
            $this->getAccount(array('uid' =>$_SESSION['uid']));
        }
    }

    /**
     * fonction permettant de charger les attributs d'un account 
     * @param  array  $fields tableau filtre pour la requête SQL
     */
    function            getAccount($fields = array())
    {
        $resultat = current($this->m->user->getUsers($fields));

        if (isset($resultat['uid']) && $resultat['uid'] != null) {
            $this->uid = $resultat['uid']; 
            $this->facebookId = $resultat['facebookId'];
            $this->twitterId = $resultat['twitterId'];
            $this->points = $resultat['points'];
        }      
    }
};
?>
