<?php
class					Controler
{
	var					$m; //model
	var					$v; //view
	var					$t; //twig
	var					$action; //action (url)
	var					$query; //query (url)
	var					$template; //nom du template a charger

						//liste des templates disponibles, ils doivent exister dans le dossier "/templates" au format html
						//les noms de templates peuvent contenir des lettres, chiffres et tirets pour matcher avec le htaccess (rewrite_url)
						//le nom d'un template sert d'url pour y acceder
	var 				$templates = array('home', 'example', 'test', 'test-example', 'review', 'posteApplication', 'redirectDisco', 'mesApplications');

						//liste des fichiers JS (globale sur toutes les pages)
						//pour ajouter un fichier sur un template particulier, voir la vue
	var 				$js = array(
							'/lib/jquery-1.12.0.js',
							'/lib/bootstrap-3.3.6-dist/js/bootstrap.min.js',
							'/js/main.js',
						);
						//liste des fichiers CSS (globale sur toutes les pages)
						//pour ajouter un fichier sur un template particulier, voir la vue
	var 				$css = array(
							'/lib/bootstrap-3.3.6-dist/css/bootstrap.css',
							'/css/style.css',
						);

	var 				$site_name;
	var 				$base_path;
	
	var 				$destination = null;	

	public function		Controler()
	{
		global			$config;

		$this->m = new Model($this);
		

		//TWIG
		require_once 'lib/Twig-1.23.1/lib/Twig/Autoloader.php'; //chargement du moteur de template Twig
		Twig_Autoloader::register();

		$loader = new Twig_Loader_Filesystem('templates'); //on definie le chemin ou sont les templates
		$twig = new Twig_Environment($loader, array(
		    'cache' => 'templates/compilation_cache', //on definie le chemin vers le cache
		    'debug' => $config->debug,
		));

		if($config->debug)
			$twig->clearCacheFiles(); //suppression de tous les fichiers de cache

		$this->t = $twig;

		$this->v = new View($this->m, $this->t);

		$this->site_name = $config->site_name;
		$this->base_path = 'http://' . $_SERVER['HTTP_HOST'] . str_replace('index.php', '', $_SERVER['SCRIPT_NAME']);

		//twitter
		include('/lib/twitter/twitteroauth.php');
		define("CONSUMER_KEY", "krDPlkmOVF3fr7wSYUuqddDdE");
		define("CONSUMER_SECRET", "B2kMWygrsckMoQoPLDl3qEYeQQIjXcAMOZ4pxszoWQsDjVF0yt");
		define("OAUTH_CALLBACK", $this->base_path . '?q=loginTwitter');	

		//facebook
		require_once '/lib/facebook/src/Facebook/autoload.php';

		// $this->m->dsm(realpath('index.php'));

		$this->action = (isset($_GET['a']) && !empty($_GET['a'])) ? $_GET['a'] : 'home'; //action : page / template a charger. Par defaut le template "home"

		$this->query = (isset($_GET['q']) && !empty($_GET['q'])) ? $_GET['q'] : false;

		// if(isset($_GET) && !empty($_GET))
		// 	$this->m->dsm($_GET, '$_GET');
		// if(isset($_POST) && !empty($_POST))
		// 	$this->m->dsm($_POST, '$_POST');

		$this->checkAction();
		$this->checkQuery();

		if(isset($_SESSION['oauth_token']) && isset($_SESSION['oauth_token_secret'])) {
			
			$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
			
			$twitterInfos = $connection->get('account/verify_credentials');

			// $this->m->dsm($twitterInfos, 'twitterInfos');
		}

		$comment = '?a=' . $this->action;
		// $this->m->db->logNew($this->template, $comment);

		if($this->destination != null)
			$this->goToDestination($this->destination);

		// if(!$this->checkAccess())
		// 	$this->template = '403';

		//chargement de la page en html via la vue
		$this->v->display($this->template, $this->css, $this->js);
	}

	// on verifie que l'action existe, sinon on affiche que la page est introuvable (template "404")
	function			checkAction()
	{
		if(in_array($this->action, $this->templates))
			$this->template = $this->action;
		else
			$this->template = '404';

	}

	function			checkQuery()
	{

		if($this->query == 'loginFacebook') {
			
			$_SESSION['isLoggedOnFacebook'] = false;

				//initialisation de facebook
			$fb = new Facebook\Facebook([
				'app_id' => '985952451480097',
				'app_secret' => 'ab932fc25911149eb104f05059d2bfd5',
				'default_graph_version' => 'v2.5',
			]);

			$helper = $fb->getRedirectLoginHelper();
			$permissions = ['public_profile', 'email']; // Optional permissions
			$loginUrl = $helper->getLoginUrl($this->base_path . '?q=login-callback', $permissions);

				//si l'utilisateur n'est pas connecté à Facebook, on le redirige vers la page de login Facebook
			if(!isset($_SESSION['fb_access_token'])) {
				header('Location: '.$loginUrl);
			}
				//sinon on change la variable de session isLoggedOnFacebook à TRUE
			else {
				$_SESSION['isLoggedOnFacebook'] = true;
			}	
			// var_dump($_SESSION['fb_access_token']);
			// var_dump($loginUrl);
		}

		if($this->query == 'login-callback') {
			$_SESSION['idFacebook'] = '';

				//initialisation de Facebook
			$fb = new Facebook\Facebook([
				'app_id' => '985952451480097',
				'app_secret' => 'ab932fc25911149eb104f05059d2bfd5',
				'default_graph_version' => 'v2.5',
			]);

			$helper = $fb->getRedirectLoginHelper();

				//on essaye de récupérer les tokens d'accès à Facebook, sinon on renvoie une erreur spécifique (SDK ou Graph)
			try {
				$accessToken = $helper->getAccessToken();
			} catch(Facebook\Exceptions\FacebookResponseException $e) {
				echo 'Graph returned an error: ' . $e->getMessage();
				exit;
			} catch(Facebook\Exceptions\FacebookSDKException $e) {
				echo 'Facebook SDK returned an error: ' . $e->getMessage();
				exit;
			}

				//si les tokens d'accès ne sont pas définis, on renvoie une erreur
			if (! isset($accessToken)) {
				if ($helper->getError()) {
					header('HTTP/1.0 401 Unauthorized');
					echo "Error: " . $helper->getError() . "\n";
					echo "Error Code: " . $helper->getErrorCode() . "\n";
					echo "Error Reason: " . $helper->getErrorReason() . "\n";
					echo "Error Description: " . $helper->getErrorDescription() . "\n";
				} else {
					header('HTTP/1.0 400 Bad Request');
					echo 'Bad request';
				}
				exit;
			}

			//echo '<h3>Access Token</h3>';
			//var_dump($accessToken->getValue());

			// The OAuth 2.0 client handler helps us manage access tokens
			$oAuth2Client = $fb->getOAuth2Client();

			// Get the access token metadata from /debug_token
			$tokenMetadata = $oAuth2Client->debugToken($accessToken);
			
			//echo '<h3>Metadata</h3>';
			//var_dump($tokenMetadata);

				//validation de l'id de l'application, retourne une erreur de type FacebooSDKException si il y a
			$tokenMetadata->validateAppId('985952451480097');
				//validation de l'id de l'utilisateur
			$tokenMetadata->validateUserId($tokenMetadata->getField('user_id'));
				//récupération de l'id Facebook dans la variable de session idFacebook
			$_SESSION['idFacebook'] = $tokenMetadata->getField('user_id');

			//chargement des données membres de la classe account
			$this->m->user->newUser("facebook", $_SESSION['idFacebook']);
			$resultat = current($this->m->user->getUsers(array('facebookId' => $_SESSION['idFacebook'])));
			$_SESSION['uid'] = $resultat['uid'];

			$tokenMetadata->validateExpiration();
			
				//échange un token avec une petite durée de vie contre un token avec une grande durée de vie
			if (! $accessToken->isLongLived()) {
				try {
					$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
				} catch (Facebook\Exceptions\FacebookSDKException $e) {
					echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
					exit;
				}
				//echo '<h3>Long-lived</h3>';
				//var_dump($accessToken->getValue());
			}
			$_SESSION['isLoggedOnFacebook'] = true;
			$_SESSION['fb_access_token'] = (string) $accessToken;

			//echo "tokenMetadata";
			// var_dump($tokenMetadata);

			$this->destination = $this->base_path;
		}

		if ($this->query == 'loginTwitter') {		
			//isLoggedOnTwitter est du type bool
			$isLoggedOnTwitter = false;
			
				//vérification si l'utilisateur est connecté à twitter sur son navigateur
			if(!empty($_SESSION['access_token']) && !empty($_SESSION['access_token']['oauth_token']) && !empty($_SESSION['access_token']['oauth_token_secret'])) {
			
				$access_token = $_SESSION['access_token'];
				$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
				
				$twitterInfos = $connection->get('account/verify_credentials');
				$isLoggedOnTwitter = true;
			}
			elseif(isset($_REQUEST['oauth_token']) && $_SESSION['oauth_token'] === $_REQUEST['oauth_token']) {
				
				$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
				$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);
				
				$_SESSION['access_token'] = $access_token;
				unset($_SESSION['oauth_token']);
				unset($_SESSION['oauth_token_secret']);
				
				if(200 == $connection->http_code) {
					$twitterInfos = $connection->get('account/verify_credentials');
					$isLoggedOnTwitter = true;
				}
				else {
					$isLoggedOnTwitter = false;
				}
			}
			else {
				$isLoggedOnTwitter = false;
			}



				//récupération de la variable isLoggedOnTwitter dans une variable de session
			$_SESSION['isLoggedOnTwitter'] = $isLoggedOnTwitter;	
			
				//si l'utilisateur est déjà connecté à twitter on le renvoie sur la page d'accueil en insérant ses données dans la BDD
			if($isLoggedOnTwitter) {
				$_SESSION['userInfos'] = $twitterInfos;

					//chargement des données membres de la classe account
				$this->m->user->newUser("twitter", $_SESSION['userInfos']->id);
				$resultat = current($this->m->user->getUsers(array('twitterId' => $_SESSION['userInfos']->id)));
				$_SESSION['uid'] = $resultat['uid'];

				$this->destination = $this->base_path;
				}
				
				// si l'utilisateur n'est pas connecté à twitter, on le renvoie vers la page de connexion Twitter
			else {
				$this->destination = $this->base_path . '?q=redirect';
			}
		}

		if ($this->query == 'redirect') {

			//connection est du type TwitterOAuth
			$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
			
			$urlRedi = OAUTH_CALLBACK;
						
			//récupération des tokens Twitter
			$request_token = $connection->getRequestToken($urlRedi);

			// $this->m->dsm($request_token, 'request_token');
			// exit();
			
			$_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
			$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];

				//soit la connexion à Twitter fonctionne, soit elle affiche un message d'erreur
			switch($connection->http_code) {
				case 200:
					$url = $connection->getAuthorizeURL($token);

					$this->destination = $url;
					break;
				default:
					echo 'Impossible de se connecter à twitter ... Merci de renouveler votre demande plus tard.';
					break;
			}
		}

		if($this->query == 'logout') {
			session_destroy(); 
			$this->m->setMessage('Déconnexion réussie', 'success');
			$this->destination = $this->base_path;
		}
	}

	//defini les regles d'accès au page
	function 			checkAccess()
	{
		$access_allowed = false; //false: par defaut tout les access sont refusee

		//pages publiques
		if(in_array($this->template, array('home', 'mentions-legales')))
			$access_allowed = true;

		if($this->m->user->logged)
			$access_allowed = true;

		return $access_allowed;
	}

	function 			goToDestination($url)
	{
		header('location: ' . $url);
		exit();
	}

};
?>
