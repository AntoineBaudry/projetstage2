<?php
/**
 * cette classe permet de faire tous les traitements dont on a besoin
 */
class                   Model
{
    /**
     * controler
     * @var controler
     */
    var                 $c;
    /**
     * database
     * @var database
     */
    var                 $db;
    /**
     * user
     * @var user
     */
    var                 $user;
    /**
     * account est défini si un utilisateur est connecté
     * @var account
     */
    var                 $account;

    var                 $messages = array();
              
    /**
     * controler de la classe model
     * @param controler &$controler controler
     */
    function            Model(&$controler)
    {
        $this->c = $controler;
        $this->db = new Database($this);
        $this->user = new User($this);
        $this->account = new Account($this);
        
       // $this->dsm($_GET);
       // $this->dsm($_SERVER);
       // $this->dsm($this->db->logLoad());
    }

    /**
     * affiche un message sur le site
     * @param string $message texte a afficher
     * @param string $type    definit la couleur du message parmis [success, warning, danger, info]
     */
    function            setMessage($message, $type = 'success')
    {
        if($type == 'error') 
            $type = 'danger';

        // $this->messages[] = array(
        $_SESSION['messages'][] = array(
            'title' => '',
            'type' => $type,
            'text' => $message
        );
    }

    // DEBUG
    // debug set message (dsm)
    //affiche un debug dans les messages
    function            dsm($var, $title = '')
    {
        global $config;

        //capture de la sortie de la function krumo
        ob_start();
        krumo($var);
        $output = ob_get_contents();
        ob_end_clean();

        if($config->debug && $config->dsm)
            // $this->messages[] = array(
            $_SESSION['messages'][] = array(
                'title' => $title,
                'type' => 'success',
                'text' => $output
            );
    }

    // TEST
    function            testPage()
    {
        $this->dsm('Test Page exemple de message');
        $output = array(
            'title' => 'Page test',
            'current_date' => date('d/m/Y H:i:s'),
            'random' => str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'),
            'group' => 'group-1',
            'items' => array(1, 2, 3, 4, 5, 6)
        );

        return $output;
    }

    /**
     * fonction permettant de gérer les 2 champs à remplir pour 
     * ajouter une application, ajoute l'application à la base de données
     * si les champs sont corrects et que l'application n'est pas déjà dans la base de données
     * par rapport à son URL
     * @param  string $nom  nom de l'application
     * @param  string $lien lien vers l'Appstore de l'application
     * @return array  $form tableau contenant la valeur des champs et un booleen error
     */
    function            posteApplication($nom, $lien) {
        $form = array(
            'form' => array(
                'nom' => array(
                    'value' => $nom,
                    'error' => false,
                ),
                'lien' => array(
                    'value' => $lien,
                    'error' => false,
                ),
            )
        );
        if (!empty($nom) && !empty($lien)) {
            $verificationLien = @get_headers($lien,1);
            if ($verificationLien[0] == 'HTTP/1.0 200 OK') {

                $liens = explode('/', $lien);
                $lienNom = '%'.$liens[5].'%';
                $application = $this->db->getApplicationByLien($lienNom);

                if ($application == null) {

                    $this->db->posteApplication($nom, $lien);
                    $this->setMessage('Application ajoutée');
                }
                else {
                    $this->setMessage('L\'application a déjà été ajoutée.', 'error');
                }
            }
            else {
                $this->setMessage('URL non valide', 'error');
                $form['form']['lien']['error'] = true;
            }

        }
        else {
            $this->setMessage('Veuillez remplir tous les champs', 'error');
            $form['form']['nom']['error'] = true;
            $form['form']['lien']['error'] = true;
        }
        return $form;
    }

    /**
     * fonction retournant les informations de l'Appstore
     * des applications
     * @param  array  $fields tableau de filtre pour la requête SQL
     * @return array  $d   tableau contenant des informations sur des applications
     */
    function            getApplications($fields = array()) {
        $result = $this->db->getApplications($fields);
        $tblapp=array();
        $tblapp[]=$result;
        
        $app=0;
        $d = array();

        foreach ($tblapp[$app] as $value) {
            // url de lecture des données appstore sur l'appli
            $AppId = $this->between('id', '?', $value['lien'] );
            $url="https://itunes.apple.com/lookup?id=".$AppId; 
            // recupère les données AppStore
            $jsonApp = file_get_contents($url); 
            $arr = json_decode($jsonApp,true);
             // stocke les données dans le tableau $arr
            $d[]=$arr;
            $app++;
        }
        return $d;
    }

    /**
     * fonction retournant une application aléatoire et ses infos de l'Appstore
     * parmi les applications qui sont éligibles à une review
     * @return array tableau contenant une application et ses infos
     */
    function            getReview() {
        $applicationReview = null;
        $tableauApplicationReview = array();
        $d = null;

        $lesApplications = $this->db->getApplicationForReview($this->account->uid);
        
        $fields = array('uid' => $this->account->uid);
        $getReview = $this->db->getReview($fields);

        if ($lesApplications != null) {
            foreach ($lesApplications as $application) {
            $reviewBool = false;
                foreach ($getReview as $review) {
                    if ($review['aid'] == $application['aid']) {
                          $reviewBool = true; 
                    }
                }
                if ($reviewBool == false) {
                    $tableauApplicationReview[] = $application;
                }
            }

            $random = rand(0, count($tableauApplicationReview)-1);

            if (count($tableauApplicationReview) > 0) {
                $applicationReview = $tableauApplicationReview[$random];

                $AppId = $this->between('id', '?', $applicationReview['lien'] );
                $url="https://itunes.apple.com/lookup?id=".$AppId; 
                // recupère les données AppStore
                $jsonApp = file_get_contents($url); 
                $arr = json_decode($jsonApp,true);
                 // stocke les données dans le tableau $arr
                $d[]=$arr;
            }      
        }
    
        return array(
            'infoApplication' => $d, 
            'application' => $applicationReview
        );
    }

    /**
     * fonction permettant d'ajouter la review à la base de données,
     * d'ajouter un point à l'utilisateur qui a fait la review
     * et de retirer un point à le créateur de l'application
     * @param  string $uid identifiant unique de l'account connecté
     * @param  string $aid identifiant unique de l'application
     */
    function            doReview($uid, $aid) {
        $this->db->ajouterReview($aid, $this->account->uid);
        $this->db->ajouterPoint($this->account->uid);
        $this->db->enleverPoint($uid);
    }

    /**
     * fonction permettant de supprimer une application du site
     * @param  string $lien lien de l'application
     */
    function            deleteApplication($lien) {
        $liens = explode('/', $lien);
        $lien = '%'.$liens[5].'%';

        $application = $this->db->getApplicationByLien($lien);
        if ($application != null) {
            $application = current($application);
            $this->db->supprimerApplication($application['aid']);
            $this->dsm($application, "application à supprimer");
        }
        


    }


    function after ($this1, $inthat) {
        if (!is_bool(strpos($inthat, $this1)))
        return substr($inthat, strpos($inthat,$this1)+strlen($this1));
    }

    function after_last ($this1, $inthat) {
        if (!is_bool(strrevpos($inthat, $this1)))
        return substr($inthat, strrevpos($inthat, $this1)+strlen($this1));
    }

    function before ($this1, $inthat) {
        return substr($inthat, 0, strpos($inthat, $this1));
    }

    function before_last ($this1, $inthat) {
        return substr($inthat, 0, strrevpos($inthat, $this1));
    }

    function between ($this1, $that, $inthat) {
        return $this->before($that, $this->after($this1, $inthat));
    }

    function between_last ($this1, $that, $inthat) {
     return $this->after_last($this1, $this->before_last($that, $inthat));
    }

    // use strrevpos function in case your php version does not include it
    function strrevpos($instr, $needle) {
        $rev_pos = strpos (strrev($instr), strrev($needle));
        if ($rev_pos===false) return false;
        else return strlen($instr) - $rev_pos - strlen($needle);
    }


    function tweet(){


    $link = "http://twitrss.me/twitter_user_to_rss/?user=SixFellows";
    
    $xml = file_get_contents($link, false);
    $xml = simplexml_load_string($xml) ;//or die("Error: Cannot create object");
    
    $tbl   = array();
    $tbl[] = $xml;
    
/*    print_r('<pre>');
    print_r($xml);
    print_r('</pre>');*/
    
    $twitter = array();
    for ($a = 0; $a < 5; $a++) {
        (string) $title = $xml->channel[0]->item[$a]->title;
        //On check les liens
        $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        
        
        if (preg_match($reg_exUrl, $title, $url)) {
            $urlcorrect = explode(" …", $url[0]);
            
            $urlcorrect = trim($urlcorrect[0]);

            $texte      = preg_replace($reg_exUrl, '<a href="' . $urlcorrect . '" target="blank">' . $urlcorrect . '</a>', $title) . '';
            
        } else {
            $texte = $title;
        }
        
        // on check les lien pic twitter
        $reg_exUrl2 = "/(pic.)[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
        
        if (preg_match($reg_exUrl2, $texte, $url2)) {
            
            $texte2 = preg_replace($reg_exUrl2, '<a href="http://' . $url2[0] . '" target="blank" >' . $url2[0] . '</a>', $texte) . '';
            
        } else {
            
            $texte2 = $texte;
            
        }
        // on check les # 
        $reg_exUrl3 = '/#([^\s]+)/';
        $reg_exUrl4 = '/(?:#[\w-]+\s*)+$/';
        
        if (preg_match_all($reg_exUrl3, $texte2, $url3)) {
                $htag = "";
                for($i = 0; $i < count($url3[0]); ++$i) {
                    $htagremove = explode("#", $url3[0][$i]);
                    $htag .= '<a href="https://twitter.com/hashtag/' . $htagremove[1] . '?src=tren" target="blank">' . $url3[0][$i] . '</a> ';
                }
            // On retire le # pour les liens
            
            $texte3     = preg_replace($reg_exUrl4, $htag, $texte2) . '';
            
        } else {
            
            $texte3 = $texte2;
            
        }
        
        // on balance tout dans un tableau
        $twitter[$a] = $texte3;
        
    }
    return $twitter;
}




};
?>
