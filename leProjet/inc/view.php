<?php
/**
 * cette classe permet de gérer l'affichage de variable dans le template HTML
 * en appelant des méthodes du model
 */
class					View
{
	/**
	 * model
	 * @var model
	 */
	var					$m;
	/**
	 * template
	 * @var template
	 */
	var					$t;

	/**
	 * context contient toutes les variables envoyées par la vue
	 * @var array
	 */
	var 				$context; //contient toutes les variables envoyees par la vue

	/**
	 * constructeur de la classe View
	 * @param model &$model model
	 * @param Twig_Environment $twig twig
	 */
	function			View(&$model, $twig)
	{
		$this->m = $model;
		$this->t = $twig;
	}



	/**
	 * 	cette fonction cherche une fonction propre au template a afficher
	 *	Le format des fonctions a appeler doit etre du type "getPageX" ou X est le nom du template en minuscule
	 *	il est possible d'ajouter et de modifier les variables du context
	 * @param  string $template template à charger
	 */
	function 			loadContent($template)
	{
		$fonction_template = $this->getFonctionTemplate($template);
		$this->m->dsm($fonction_template, 'Fonction de template possible:');
		$template_datas = array();
		if(method_exists($this, $fonction_template))
		{
			$getPage = $fonction_template;
			$template_datas = $this->$getPage();
		}
		
		$this->context['content'] = $template_datas;
	}

	/**
	 * fonction permettant de retourner le nom de la fonction à utiliser pour le template
	 * @param  string $template nom du template
	 * @return string   $out  le nom de la fonction à utiliser
	 */
	function 			getFonctionTemplate($template)
	{
		$out = 'getPage';
		$e = explode('-', $template);
		foreach ($e as $key => $value) {
			$out .= ucfirst($value);	
		}

		return $out;
	}

	//une fonction de template peut charger depuis le model des variables pour l'affichage
	function 			getPageTest()
	{
		return $this->m->testPage();
	}

	//une fonction de template peut charger depuis le model des variables pour l'affichage
	function 			getPageHome()
	{	
		$home = array(
			'title' => "Accueil",
		);
		return $home;
	}

	function 			getPageTestExample()
	{
		return array(
			'author' => 'Fabien',
			'group' => 'group-1',
			'donnees' => array(1,2,3,4,5,6)
		);
	}


	/**
	 * fonction permettant de vérifier les champs de saisis. 
	 * @return array contenant la vérification des champs saisis
	 */
	function 			getPagePosteApplication()
	{
		$form = array();
		if (isset($_POST['envoyer'])) {
			$form = $this->m->posteApplication($_POST['nomApplication'], $_POST['lien']);
		}

		return $form;
	}

	/**
	 * fonction permettant de retourner les informations d'une application
	 *  au template pour qu'elle l'affiche.
	 * @return array contenant les informations de l'application à afficher
	 */
	function 			getPageReview()
	{
		if (isset($_SESSION['application'])) {
			$application = $_SESSION['application'];
		}
		
		if (isset($_POST['submit'])) {
			if ($this->m->account->uid != null) {
				$this->m->doReview($application['uid'], $application['aid']);
				if ($_FILES['fichier']['error'] == 0) {
					$chemin = 'data/'.$this->m->account->uid;
					if (!file_exists($chemin)) {
						mkdir($chemin);
					}
					$nom = $application['nom']."_Review";
					$type = $_FILES['fichier']['type'];
					$maChaine = explode('/', $type);
					$fichier = $nom.'.'.$maChaine[1];
					move_uploaded_file($_FILES['fichier']['tmp_name'],"$chemin/$fichier");
					unset($_SESSION['application']);
				}	
			}
		}

		$application = null;
		$resultat = $this->m->getReview();

		if (current($resultat) != null) {
			$infoApplication = current(current([$resultat][0]));
			$application = $resultat['application'];
			$_SESSION['application'] = $application;
		}
		else {
			$infoApplication = null;
		}

		return array(
			'title' => "Review",
			'application' => $infoApplication,
		);
	}


	/**
	 * fonction permettant de retourner les applications de tous les users
	 * ou les applications avec un filtre au template.
	 * @return array contenant des applications
	 */
	function 			getPageMesApplications() {
		
		if (isset($_POST['supprimer'])) {
			$this->m->deleteApplication($_POST['supprimer']);
		}

		$resultat = $this->m->getApplications(array('uid' =>$this->m->account->uid));
		return array(
			'title' => 'Liste d\'application',
			'resultat' => $resultat
		);	
	}


	/**
	 * fonction qui gere l'affichage d'une page
	 * @param  string $template le template à afficher
	 * @param  array  $css      les fichiers css ajoutés dans le controler
	 * @param  array  $js       les fichiers js ajoutés dans le controler
	 */
	function			display($template, $css = array(), $js = array())
	{
		//l'objet context contient toutes les variables utilisees par les templates
		//des variables sont disponibles par defaut (hors loadContent()) pour tous les templates
		$this->context = array(
			'head' => array(
				'css' => $css,
				'js' => $js,
			),
			'base_path' => $this->m->c->base_path,
			'title' => $this->m->c->site_name, //titre de la page par defaut
			'template' => $template, //nom du template de la page
			'template_file' => $template . '.html', //template qui sera charge dans html.html
			'account' => $this->m->account,
			'tweets' => $this->m->tweet()
		);

		$this->loadContent($template); //on charge le contenu (les variables) de la page si elles existes

		//ajout du titre sur site sur les noms de page
		if(isset($this->context['content']['title']))
			$this->context['title'] = $this->m->c->site_name . ' | ' . $this->context['content']['title'];

		$this->m->dsm($this->context, 'CONTEXT');


		// $this->context['messages'] = $this->m->messages;
		$this->context['messages'] = $_SESSION['messages'];
		$_SESSION['messages'] = array();
		
		// $this->m->dsm($GLOBALS);
		// $this->m->dsm($this->context);
		// $this->m->dsm($template);

		echo $this->t->render('html.html', $this->context);
	}
};
?>
