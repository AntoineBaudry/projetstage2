<?php
/**
 * cette classe permet de gérer tous les users du site
 */
class                   User
{
    /**
     * model
     * @var model
     */
    var                 $m;
    /**
     * database
     * @var database
     */
    var                 $db;
        
    /**
     * constructeur de la classe User permettant de charger les attributs (model et database)
     * @param model &$model model
     */
    function            User(&$model)
    {
        $this->m = $model;
        $this->db = $this->m->db;
    }

    /**
     * fonction permettant d'ajouter à la base de données un utilisateur
     * qui se connecterait pour la première fois sur le site
     * @param  string $type source de la connexion (Facebook/Twitter)
     * @param  string $id   le facebook/twitter ID de la personne connectée
     * @return bool $estAjouté Vrai si l'utilisateur a été ajouté, Faux si l'utilisateur était déjà dans la base de données
     */
    function            newUser($type, $id)
    {
        $estAjouté = false;
        $fields = array();
        if ($type == 'facebook') {
            $fields = array('type' => $type, 'facebookId' => $id);
        }
        elseif ($type == 'twitter') {
            $fields = array('type' => $type, 'twitterId' => $id);
        }
        
        $resultat = $this->getUsers($fields);
        if (count($resultat) == 0) {
            $this->db->newUser($type, $id);
            $estAjouté = true;
        }
        return $estAjouté;
    }
    
    /**
     * fonction permettant de récupérer tous les accounts qui
     * sont dans la base de données ou récupérer un account précis
     * avec un filtre ($fields)
     * @param  array  $fields tableau filtre pour la requête SQL
     * @return array         tableau contenant de 0 à plusieurs accounts
     */
    function            getUsers($fields = array())
    {
        return $this->db->getUsers($fields);
    }
};
?>
