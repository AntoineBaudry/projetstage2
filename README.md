### Cas d'utilisation :  ###

![casUtilisation.png](https://bitbucket.org/repo/6pG55k/images/1293227560-casUtilisation.png)


### Base de données : ###

![bdd.png](https://bitbucket.org/repo/6pG55k/images/234816319-bdd.png)


### Contexte ###

Mon projet est le développement d'un site web qui permettait à un développeur d'application iOS de : 

* poster son application sur le site 
* de tester des applications d'autres développeurs et de noter sur l'AppStore l'application testée
* recevoir des avis sur ses applications

Le tout avec un système de points

### Outils et logiciels utilisés :###

* WAMP
* Sublime Text

### Description de l'activité : ###

La première mission effectuée sur ce projet était la conception de la base de données et le diagramme de cas d'utilisation pour bien cerner les besoins qu'on m'a demandé.

Ensuite J'ai effectué le design général du site à l'aide d'un template Photoshop préconçu : j'ai reproduit le maximum d'éléments avec les bases de Bootstrap et découper avec Photoshop les éléments étaient non reproductible. 

![fichierPhotoshop.png](https://bitbucket.org/repo/6pG55k/images/3078455344-fichierPhotoshop.png)


Puis j'ai effectué la connexion sur le site via Twitter/Facebook à l'aide des APIs. 

J'ai crée sur Facebook et Twitter une "application" pour l'identification.

Une fois l'identification faite, je récupère des données de la base de données pour alimenter ma classe Account.

![creationAppliTwitter.png](https://bitbucket.org/repo/6pG55k/images/410976948-creationAppliTwitter.png)


Ensuite, j'ai créer les différentes pages :

* Une page pour poster une application, l'utilisateur passe seulement le nom de l'application et le lien vers l'AppStore que je stocke dans la base de données.
* Une page faisant la liste des applications postées par l'utilisateur en cours (connecté) où il pourra les gérer (supprimer/modifier)
* Une page où une application va être choisie au hasard pour être testée



Tout le système fonctionne avec des points pour que les utilisateurs qui testent beaucoup d'application soient récompensés : 

​l'utilisateur commence avec 0 point, il poste une application, son application ne pourra pas être testé car il a 0 point. Pour gagner des points, il faudra qu'il teste l'application d'un autre utilisateur. Quand un autre utilisateur testera son application, il va perdre 1 point pour qu'il continue à tester d'autres applications. 



Pour vérifier que l'utilisateur a bien testé l'application, une capture d'écran doit être envoyé sur le site montrant qu'il a bien mis les étoiles sur l'AppStore avec un commentaire.